void main() {
  for (int i = 0; i<5; i++)
    print("hello ${i+1}");
  
    //variable declarations
    var inferredString = "Hello";
    String explicitString = "World";
  
    print(inferredString + explicitString);

    //null safety ; ?declaration
    int? newNumber;
    print(newNumber);
    newNumber = 42;
    print(newNumber);

    //late modifier
    late int new2Number;
    new2Number = 4;
    print(new2Number);

    //accessing nullable variables
    int? goals;
    if(goals !=null) {
      print(goals + 2);
    }

    //null-aware methods
    String? goalScorer;
    print(goalScorer?.length);

    //null-accertion operator
    int? goalTime;
    bool goalScorered = false;
    if(goalScorered) {
        goalTime = 21;
        goalScorer = "Bobby";
    }
    if(goalTime !=null) {
        print(goalScorer!.length);
    }

    //List
    List dynamicList = [];
    print(dynamicList.length);
    dynamicList.add("Hello");
    print(dynamicList[0]);
    print(dynamicList.length);

    List preFilledDynamicList = [1,2,3];
    print(preFilledDynamicList[0]);
    print(preFilledDynamicList.length);

    List fixedList = List.filled(3, "World");
    //fixedList.add("Hello"); //error
    fixedList[0] = "Hello"; 
    print(fixedList[0]);    //print Hello
    print(fixedList[1]);    //print World

    //Maps
    Map nameAgeMap = {};
    nameAgeMap["Alice"] = 23;
    print(nameAgeMap["Alice"]); //print 23

    //String
    String singleQuoteString = 'here is a single quote string';
    String doubleQuoteString = 'here is a double quote string';
    String multiLineString = '''here is a multi-line single quote string''';
    String str1 = 'here is a ';
    String str2 = str1 + 'concatenated string';
    print(str2); // here is a concatenated string

    //String interpolation
    String someString = "Happy string";
    print("The string is: $someString"); // prints The string is: Happy string
    print("The string length is: ${someString.length}"); // prints The string length is: 16
    print("The string length is: $someString.length");  // prints The string length is: Happy string.length

    //Const and final
    // a variable that will not change in value can be defined as a const
    // value is not known as compile time which can use final 

    // if else
    String test = "test2";
    if (test == "test1") {
      print("Test1");
    } else if (test == "test2") {
      print("Test2");
    } else {
      print("Something else");
    } //prints test 2

    if (test == "test2") print("Test2 again"); // prints Test 2

    //While and do while loops
    int counter = 0;
    while (counter < 2) {
      print(counter);
      counter++;
    } // Prints 0, 1

    do {
      print(counter);
      counter++;
    } while (counter < 2); // Prints 2

    //For loops
    for (int index = 0; index < 2; index++) {
      print(index);
    }  //prints 0 1

    //Break and continue
    int counter2 = 0;
    while (counter2 < 10) {
      counter2++;
      if (counter2 == 4) {
        break;
      } else if (counter2 == 2) {
        continue;
      }
      print(counter2);
    } // print 1, 3

    //Functions and methods
    var helloFunction = sayHello();
    String helloMessage = helloFunction;
    print(helloMessage); //prints Hello world!
}

String sayHello() {
  return "Hello world!";
}