import 'package:flutter/material.dart';
import 'package:flutter_app_myimageviewer/model/image_model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class App extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return AppState();
  }

}

class AppState extends State<App> {
  int counter = 1;
  List<ImageModel> images = [];

  fetchImages() async {
    // counter++;
    // var url = Uri.https('jsonplaceholder.typicode.com', 'photos/$counter');
    // var response = await http.get(url);
    //
    // print(response.body);
    //
    // var jsonObject = json.decode(response.body);
    // var imageModel = ImageModel(jsonObject['id'], jsonObject['url']);
    // images.add(imageModel);
    //
    //
    // print('counter=$counter');
    // print('Length of images=${images.length}');
    //
    // setState(() {
    //   // Do nothing
    // });
    print('counter=$counter');
      Uri url = Uri.https('jsonplaceholder.typicode.com','photos/$counter');
      var response = await http.get(url);
      print(response.body);
      var jsonObject = json.decode(response.body);
      var imageModel = ImageModel(jsonObject['id'], jsonObject['url']);
      images.add(imageModel);
      print(imageModel);
      print(images);

      setState(() {
        counter++;
      });
  }

  @override
  Widget build(BuildContext context) {
    var appWidget = new MaterialApp(
        home: Scaffold(
          appBar: AppBar(title: Text('My Image Viewer - v0.0.6'),),
          body: ListView.builder(
            itemCount: images.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                title: Image.network(images[index].getUrl),
              );
            },
          ),
          floatingActionButton: FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: fetchImages
          ),
        )
    );

    return appWidget;
  }
}